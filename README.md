# Containers/obs-studio

Unofficial image for running [OBS Studio][obs-studio] in distrobox.  This image
is based on the latest available [openSUSE Tumbleweed][opensuse-tumbleweed]
[image][opensuse-tumbleweed-image] with the [community built OBS
Studio][open-build-service-obs-studio] pre-installed.

The OBS Studio package comes with AV1 VAAPI (hardware) encoding, software AV1
encoding, JACK, pipewire, pulseaudio and wayland support.  All default plugins
except for the ones listed later are also included.  Uses the standard
Tumbleweed ffmpeg package (run `ffmpeg -codecs 2> /dev/null | grep av1_vaapi` to
check if VAAPI encoding is likely to work for you).

obs-x264, HEVC, obs-browser and AJA plugins are NOT included.  The package works
well with AV1.  h264 and h265 are not supported.

## Usage

```
distrobox create -n obs-studio -i registry.gitlab.com/mareksapota-public/containers/obs-studio
distrobox enter obs-studio -- obs-studio
```

## Updates

This image auto updates on a [weekly schedule][ci-schedule] based on the latest
available [opensuse/tumbleweed:latest][opensuse-tumbleweed-image] image and the
latest available [OBS Studio][open-build-service-obs-studio] package.

To update an existing distrobox run:

```
distrobox upgrade obs-studio
```

# Notice

This image is unofficial and associated with neither the [openSUSE][opensuse]
project nor the [OBS studio][obs-studio] project.

As with many OCI images, this image contains software under various licenses
(such as any packages from the base distribution and/or indirect dependencies of
the primary software being contained).  This image follows openSUSE guidelines
for [accepted licenses][opensuse-accepted-licenses] and [format
restrictions][opensuse-restricted-formats].  For a full list of licenses for
software included in this image run `zypper licenses` inside of the container.
As for any pre-built image usage, it is the image user's responsibility to
ensure that any use of this image complies with any relevant licenses for all
software contained within.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

[opensuse]: https://www.opensuse.org/
[opensuse-accepted-licenses]: https://en.opensuse.org/openSUSE:Accepted_licences
[opensuse-restricted-formats]: https://en.opensuse.org/Restricted_formats
[ci-schedule]: https://gitlab.com/mareksapota-public/containers/obs-studio/-/pipeline_schedules
[opensuse-tumbleweed-image]: https://registry.opensuse.org/cgi-bin/cooverview?srch_term=project%3D%5EopenSUSE%3AContainers%3A+container%3D%5Eopensuse%2Ftumbleweed%24
[podman]: https://podman.io/
[obs-studio]: https://obsproject.com/
[open-build-service-obs-studio]: https://build.opensuse.org/package/show/home:mareksapota/obs-studio
